from rest_framework import serializers
from places.models import Feedback, Place
from users.models import User


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ('__all__')


class UserSerializer(serializers.ModelSerializer):
    place_set = PlaceSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'place_set']


class FeedbackSerializer(serializers.ModelSerializer):
    author = UserSerializer()

    class Meta:
        model = Feedback
        fields = ('place', 'text', 'author', 'id')
        # depth = 1х
