from places import models


def pending_feedbacks(request):
    new_feedbacks_count = None
    if request.user.is_authenticated:
        new_feedbacks_count = models.Feedback.objects.filter(
            place__in=(x for x in request.user.place_set.all()),
            status=models.Feedback.NEW
        ).count()
    return {'NEW_FEEDBACKS': new_feedbacks_count}
