from django import forms

from places import models


class PlaceForm(forms.ModelForm):
    class Meta:
        model = models.Place
        fields = ('name', 'description', 'image')


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = models.Feedback
        fields = ('place', 'text')


class CouponForm(forms.ModelForm):
    class Meta:
        model = models.Coupon
        fields = ('code', 'description', 'price', 'place')
