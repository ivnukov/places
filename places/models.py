from django.db import models

from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class TimestampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Feedback(TimestampModel):
    NEW = 1
    REVIEW = 2
    DECLINED = 3
    APPROVED = 4
    EDIT_REQUIRED = 5

    STATUSES = (
        (NEW, 'NEW'),
        (REVIEW, 'REVIEW'),
        (DECLINED, 'DECLINED'),
        (APPROVED, 'APPROVED'),
        (EDIT_REQUIRED, 'EDIT_REQUIRED'),
    )

    place = models.ForeignKey('Place', on_delete=models.CASCADE, related_name='places')
    text = models.TextField()
    author = models.ForeignKey(USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL,
                               related_name='feedbacks')

    status = models.PositiveSmallIntegerField(choices=STATUSES)

    approved_by = models.ForeignKey(USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL,
                                    related_name='approved_feedbacks')
    approved_at = models.DateTimeField(blank=True, null=True)

    @property
    def total_seats(self):
        return self.rows * self.seats

    # class Meta:
    #     unique_together = ['place', 'author']


class Place(TimestampModel):
    created_by = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(blank=True, null=True, upload_to='place_images/')

    def __str__(self):
        return f"{self.name}"

    @property
    def feedback_check_required(self):
        return self.feedback_set.filter(status=Feedback.NEW).exists()


class Coupon(TimestampModel):
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    price = models.PositiveIntegerField()
    owners = models.ManyToManyField(through='UserCoupon', to=USER_MODEL, blank=True)
    code = models.CharField(max_length=80, blank=True, null=True)
    description = models.TextField(blank=True, null=True)


class UserCoupon(TimestampModel):
    coupon_id = models.ForeignKey(Coupon, on_delete=models.CASCADE)
    user_id = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    used = models.BooleanField(default=False)
    used_at = models.DateTimeField(blank=True, null=True)


from django.db.models.signals import pre_save
from django.dispatch import receiver


# @receiver(pre_save)
# def my_signal(sender, **kwargs):
#     print(sender)
#     inst = kwargs['instance']
#     if inst.id is None:
#         inst.total_seats = inst.rows * inst.seats
#
