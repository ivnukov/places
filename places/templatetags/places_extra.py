from django import template

register = template.Library()


@register.filter(name='check_row')
def check_row(value, arg):
    if arg in dict(value).keys():
        return value, arg
    else:
        return False


@register.filter(name='check_seat')
def check_seat(value, arg):
    if value:
        return (value[1], arg) in value[0]
    return value
