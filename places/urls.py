from django.urls import path

from places import views

urlpatterns = [
    path('', views.Homepage.as_view(), name='homepage'),
    path('places/new', views.NewPlaceView.as_view(), name='new_place'),
    path('places/<int:pk>', views.PlaceDetail.as_view(), name='place_detail'),
    path('places/<int:pk>/edit', views.PlaceEdit.as_view(), name='place_edit'),
    path('feedback/<int:pk>/new', views.NewFeedBackView.as_view(), name='new_feedback'),
    path('feedback/<int:pk>/moderate', views.FeedbackApprove.as_view(), name='moderate_feedback'),
    path('coupons/', views.CouponList.as_view(), name='coupons'),
    path('coupons/add/', views.NewCoupon.as_view(), name='new_coupon'),
    path('json_places/', views.PlacesAPIView.as_view(), name='user_places'),
    path('json_places/<int:pk>', views.PlacesAPIView.as_view(), name='user_places_detail'),
    path('check', views.static_view)
]
