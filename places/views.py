from django.conf import settings
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet, ModelViewSet

from places import models, forms
from places.api.permissions import AuthorOnlyPermission
from places.api.serializers import FeedbackSerializer, PlaceSerializer


class Homepage(ListView):
    model = models.Place
    template_name = 'places/base.html'


class PlaceDetail(DetailView):
    model = models.Place


class NewPlaceView(CreateView):
    model = models.Place
    form_class = forms.PlaceForm
    success_url = reverse_lazy('homepage')

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.save()
        return super().form_valid(form)


class NewFeedBackView(CreateView):
    model = models.Feedback
    form_class = forms.FeedbackForm
    success_url = reverse_lazy('homepage')

    def get_initial(self):
        initial = {'place': self.kwargs.get('pk')}
        return initial

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.status = self.object.NEW
        self.object.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, 'Invalid DATA')
        return super(NewFeedBackView, self).form_invalid(form)


class FeedbacksToApprove(ListView):
    model = models.Feedback


class FeedbackApprove(View):
    def post(self, request, *args, **kwargs):
        try:
            feedback = models.Feedback.objects.get(id=self.kwargs['pk'])
        except models.Feedback.DoesNotExist:
            return HttpResponse('Invalid feedback id', status=400)
        else:
            action = self.request.POST.get('action')
            if action == 'approve':
                feedback.status = feedback.APPROVED
                feedback.author.tokens += settings.APPROVE_REWARD
                feedback.author.save()
            elif action == 'decline':
                feedback.status = feedback.DECLINED
            feedback.save()
            return HttpResponseRedirect(reverse('place_detail', kwargs={'pk': feedback.place_id}))


class PlaceEdit(UpdateView):
    model = models.Place
    form_class = forms.PlaceForm

    def get_context_data(self, **kwargs):
        """
        For Mary <3
        """
        context = super(PlaceEdit, self).get_context_data(**kwargs)
        new_places = models.Place.objects.filter(status='NEW')
        done_places = models.Place.objects.filter(status='NEW')
        done_places = models.Place.objects.filter(status='NEW')
        """
        For Matvey po bratski
        """
        ordering = self.request.GET.get('order_by')
        context.update(dict(new_places=new_places, ordering=ordering))
        return context


    def get_success_url(self):
        return reverse('place_detail', kwargs={'pk': self.object.id})


class CouponList(ListView):
    model = models.Coupon


class NewCoupon(CreateView):
    model = models.Coupon
    form_class = forms.CouponForm
    success_url = reverse_lazy('coupons')

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if not self.request.user.is_superuser:
            form.fields['place'].queryset = User.objects.filter(id=request.user.id)
        return form


class FeedbackViewSet(ModelViewSet):
    queryset = models.Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = [IsAuthenticated, AuthorOnlyPermission]

    @action(detail=True, methods=['get', 'post'])
    def mass_delete(self):
        # object = self.get_object()
        # object.approve()
        # return Response()
        pass


class PlacesAPIView(APIView):
    def get(self, request, *args, **kwargs):
        if kwargs.get('pk'):
            place = models.Place.objects.get(id=kwargs.get('pk'))
            data = PlaceSerializer(place).data
        else:
            places = models.Place.objects.filter(created_by=request.user)
            data = PlaceSerializer(places, many=True).data
        return Response(data, status=200)


def static_view(request):
    blocked_seats = [(1, 1), (1, 3), (2, 4), (5, 8)]
    cnt = {'rows': range(1, 6), 'seats': range(1, 9), 'blocked_seats': blocked_seats}
    if request.method == 'POST':
        for key, value in request.POST.items():
            if key.startswith('seat'):
                row, seat = key.split('-')[-2:]

    return render(request, 'places/test.html', cnt)
