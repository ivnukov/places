from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class PlacesUserManager(UserManager):
    def _create_user(self, username, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        email = self.normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username=None, email=None, password=None, **extra_fields):
        super(PlacesUserManager, self).create_superuser(username, email, password, **extra_fields)


# Create your models here.
class User(AbstractUser):
    email = models.EmailField(unique=True)
    tokens = models.PositiveSmallIntegerField(default=0)

    username = models.CharField(max_length=150, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ()

    objects = PlacesUserManager()

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        if not self.username:
            self.username = self.email.lower()
        return super().save(*args, **kwargs)
