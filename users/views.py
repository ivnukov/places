from celery import Celery
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout as logout_
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse

from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from places.api.serializers import UserSerializer
from users.forms import RegisterForm, EmailConfirmForm
from users.models import User

app = Celery('log_new', broker='redis://localhost')


@app.task
def log_new():
    with open('new_user', 'a+') as fp:
        fp.write('NEW USER REGISTERED!!!')


def register(request):
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data.get('username'),
                email=form.cleaned_data.get('email'),
            )
            user.set_password(form.cleaned_data.get('password'))
            user.save()
            login(request, user)
            app.send_task(log_new)
            return redirect('/')
    return render(request, 'users/register.html', {'form': form})


def logout(request):
    logout_(request)
    return redirect('/')


class UserInfo(View, LoginRequiredMixin):
    template_name = 'users/personal_page.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ObtainTokenView(APIView):
    http_method_names = ['post']
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        user = authenticate(request,
                            email=self.request.data.get('email'),
                            password=self.request.data.get('password'))
        if user is not None:
            return Response(str(Token.objects.get(user=user)), 200)
        else:
            return HttpResponse(status=401)
